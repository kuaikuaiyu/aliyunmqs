#!/usr/bin/env python
# coding: utf-8
__author__ = 'wzy'
__date__ = '2015-07-31 11:59'


from setuptools import setup


setup(
    name='alimqs-tornado',
    version='0.0.2',
    description='AliYun MQS Client for Tornado.',
    author='wzy',
    author_email='exmail.wang@gmail.com',
    license='MIT',
    packages=['alimqs_tornado'],
    install_requires=['tornado==3.2.2'],
    keywords=['alimqs', 'tornado'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
)
