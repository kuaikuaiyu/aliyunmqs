#!/usr/bin/env python
# coding: utf-8
__author__ = 'wzy'
__date__ = '2015-07-31 11:41'


from tornado import gen
from . import *


@gen.coroutine
def main():
    queue = Queue('test', Client('host', 'access_id', 'access_key'))
    yield queue.create()
    yield queue.send_message(Message('hello world'))
    yield queue.send_message(Message('hello world delay 5', delay=5))
    while True:
        msg = yield queue.receive_message()
        if msg:
            print(msg.body)
            yield queue.delete_message(msg)
        else:
            break

if __name__ == '__main__':
    import tornado.ioloop
    tornado.ioloop.IOLoop.instance().run_sync(main)